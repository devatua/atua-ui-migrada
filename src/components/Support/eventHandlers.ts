import { ChangeEvent, Dispatch, SetStateAction } from 'react';

type EventHandler<E, S> = (event: E, set: S) => void;

export const changeEventHandler: EventHandler<
  ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  Dispatch<SetStateAction<any>>
> = (event, setState): void => {
  setState(event.target.value);
};
