/*
support imports
 */
import { changeEventHandler } from './eventHandlers';

/*
exports all functions
 */
export { changeEventHandler };
