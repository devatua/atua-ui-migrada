/*
html semantics imports
 */
import Container from './Container/Container';
import FlexContainer from './Container/FlexContainer/FlexContainer';
import Footer from './Semantic/Footer/Footer';
import Header from './Semantic/Header/Header';
import Main from './Semantic/Main/Main';
import Aside from './Semantic/Aside/Aside';
import Nav from './Semantic/Nav/Nav';
import Form from './Form/Form';

/*
exports all components
 */
export { Container, FlexContainer, Footer, Header, Main, Aside, Nav, Form };
