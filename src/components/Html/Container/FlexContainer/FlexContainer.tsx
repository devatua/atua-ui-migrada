import React from 'react';

import Container from '../Container';
import { TypeFlex } from '../index';

const flexContainer: React.FC<React.ReactNode & TypeFlex> = ({
  children,
  flexDirection,
  position,
  margin,
  padding,
  top,
  right,
  bottom,
  left,
  alignItems,
  alignContent,
  alignSelf,
  justifyItems,
  justifyContent,
  justifySelf,
  width,
  minWidth,
  height,
  minHeight,
  overflow,
  background,
  zIndex,
  style,
}) => {
  const flexStyle = {
    flexDirection: flexDirection,
    alignItems: alignItems || 'flex-start',
    alignContent: alignContent,
    alignSelf: alignSelf,
    justifyItems: justifyItems,
    justifyContent: justifyContent || 'flex-start',
    justifySelf: justifySelf,
    ...style,
  };

  return (
    <Container
      display={'flex'}
      position={position}
      margin={margin}
      padding={padding}
      top={top}
      left={left}
      bottom={bottom}
      right={right}
      width={width}
      minWidth={minWidth}
      height={height}
      minHeight={minHeight}
      overflow={overflow}
      background={background}
      zIndex={zIndex}
      style={flexStyle}
    >
      {children}
    </Container>
  );
};

export default flexContainer;
