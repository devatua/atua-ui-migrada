import {
  AlignContentProperty,
  AlignItemsProperty,
  AlignSelfProperty,
  BackgroundProperty,
  BottomProperty,
  DisplayProperty,
  FlexDirectionProperty,
  HeightProperty,
  JustifyContentProperty,
  JustifyItemsProperty,
  JustifySelfProperty,
  LeftProperty,
  MarginProperty,
  OverflowProperty,
  PaddingProperty,
  PositionProperty,
  RightProperty,
  TopProperty,
  WidthProperty,
  ZIndexProperty,
} from 'csstype';

import React from 'react';

export type TypeContainer = {
  display: DisplayProperty;
  position?: PositionProperty;
  margin?: MarginProperty<string>;
  padding?: PaddingProperty<string>;
  top?: TopProperty<string>;
  left?: LeftProperty<string>;
  bottom?: BottomProperty<string>;
  right?: RightProperty<string>;
  width?: WidthProperty<string>;
  minWidth?: WidthProperty<string>;
  height?: HeightProperty<string>;
  minHeight?: HeightProperty<string>;
  overflow?: OverflowProperty;
  background?: BackgroundProperty<string>;
  zIndex?: ZIndexProperty;
  style?: React.CSSProperties;
};

export interface TypeFlex extends Omit<TypeContainer, 'display'> {
  flexDirection: FlexDirectionProperty;
  alignItems?: AlignItemsProperty;
  alignContent?: AlignContentProperty;
  alignSelf?: AlignSelfProperty;
  justifyItems?: JustifyItemsProperty;
  justifyContent?: JustifyContentProperty;
  justifySelf?: JustifySelfProperty;
}
