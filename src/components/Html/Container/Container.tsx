import React from 'react';

import { TypeContainer } from './index';

const container: React.FC<React.ReactNode & TypeContainer> = ({
  children,
  display,
  position,
  margin,
  padding,
  top,
  left,
  bottom,
  right,
  width,
  minWidth,
  height,
  minHeight,
  overflow,
  background,
  zIndex,
  style,
}) => {
  const _style = {
    display: display,
    position: position,
    margin: margin,
    padding: padding,
    top: top,
    left: left,
    bottom: bottom,
    right: right,
    width: width,
    minWidth: minWidth,
    height: height,
    minHeight: minHeight,
    overflow: overflow,
    background: background,
    zIndex: zIndex,
    ...style,
  };

  return <div style={_style}>{children}</div>;
};

export default container;
