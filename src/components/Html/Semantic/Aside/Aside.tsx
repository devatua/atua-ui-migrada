import React, { CSSProperties, PropsWithChildren } from 'react';

const aside: React.FC<PropsWithChildren<{ style?: CSSProperties }>> = ({ children, style }) => (
  <aside style={style}>{children}</aside>
);

export default aside;
