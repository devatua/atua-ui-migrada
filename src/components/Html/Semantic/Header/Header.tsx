import React, { CSSProperties, PropsWithChildren } from 'react';

const header: React.FC<PropsWithChildren<{ style?: CSSProperties }>> = ({ children, style }) => (
  <header style={style}>{children}</header>
);

export default header;
