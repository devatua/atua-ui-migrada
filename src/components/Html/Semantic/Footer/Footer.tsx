import React, { CSSProperties, PropsWithChildren } from 'react';

const footer: React.FC<PropsWithChildren<{ style?: CSSProperties }>> = ({ children, style }) => (
  <footer style={style}>{children}</footer>
);

export default footer;
