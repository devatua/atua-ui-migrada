import React, { CSSProperties, PropsWithChildren } from 'react';

const article: React.FC<PropsWithChildren<{ style?: CSSProperties }>> = ({ children, style }) => (
  <article style={style}>{children}</article>
);

export default article;
