import React, { CSSProperties, PropsWithChildren } from 'react';

const nav: React.FC<PropsWithChildren<{ style?: CSSProperties }>> = ({ children, style }) => (
  <nav style={style}>{children}</nav>
);

export default nav;
