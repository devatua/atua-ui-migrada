import React, { CSSProperties, PropsWithChildren } from 'react';

const main: React.FC<PropsWithChildren<{ style?: CSSProperties }>> = ({ children, style }) => (
  <main style={style}>{children}</main>
);

export default main;
