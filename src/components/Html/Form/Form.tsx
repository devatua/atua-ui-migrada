import React, { FormEventHandler } from 'react';

type Props = {
  onSubmit: FormEventHandler<HTMLFormElement>;
};

const form: React.FC<Props> = ({ children, onSubmit }) => <form onSubmit={onSubmit}>{children}</form>;

export default form;
