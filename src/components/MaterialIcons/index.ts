import {
  MenuRounded,
  SearchRounded,
  EditRounded,
  DeleteRounded,
  InfoRounded,
  HelpRounded,
  SendRounded,
} from '@material-ui/icons';

export { MenuRounded, SearchRounded, EditRounded, DeleteRounded, InfoRounded, HelpRounded, SendRounded };
