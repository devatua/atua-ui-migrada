import React from 'react';

import { createMuiTheme, Theme, ThemeProvider } from '@material-ui/core/styles';

import { CssBaseline } from '@material-ui/core';

const defaultTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#2B3595',
    },
    secondary: {
      main: '#F78820',
    },
    error: {
      main: '#c6001f',
    },
  },
  // overrides: {
  //   MuiTextField: {
  //     root: {
  //       '& .MuiInputBase-root': {
  //         height: '44px'
  //       },
  //       '& .MuiInputBase-input': {
  //         height: '44px',
  //         lineHeight: '44px'
  //       }
  //     }
  //   }
  // }
});

type Props = {
  theme?: Theme;
};

const themeProvider: React.FC<Props & React.ReactNode> = ({ children, theme }) => (
  <ThemeProvider theme={theme || defaultTheme}>
    <CssBaseline />
    {children}
  </ThemeProvider>
);

export default themeProvider;
