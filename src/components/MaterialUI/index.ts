/*
material components imports
 */
import { Theme, createMuiTheme, useTheme, CssBaseline, Breadcrumbs, Link } from '@material-ui/core';
import Typography from './Typography/Typography';
import AppBar from './AppBar/AppBar';
import ThemeProvider from './ThemeProvider/ThemeProvider';
import InputText from './Input/Text/InputText';
import SelectText from './Select/SelectText';
import Button from './Button/Button';
import Fab from './Fab/Fab';

/*
exports all components and functions
 */
export {
  Typography,
  AppBar,
  ThemeProvider,
  createMuiTheme,
  useTheme,
  CssBaseline,
  InputText,
  SelectText,
  Button,
  Fab,
  Breadcrumbs,
  Link,
};

export type MaterialTheme = Theme;
