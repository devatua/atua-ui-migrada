import React from 'react';
import { Fab } from '@material-ui/core';
import { FabProps } from '@material-ui/core/Fab';

const fab: React.FC<React.ReactNode & FabProps> = ({
  children,
  color,
  disabled,
  disableFocusRipple,
  disableRipple,
  href,
  size,
  variant,
  onClick,
}) => (
  <Fab
    color={color}
    disabled={disabled}
    disableFocusRipple={disableFocusRipple}
    disableRipple={disableRipple}
    href={href}
    size={size}
    variant={variant}
    onClick={onClick}
  >
    {children}
  </Fab>
);

export default fab;
