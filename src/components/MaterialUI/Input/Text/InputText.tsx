import React from 'react';
import TextField, { TextFieldProps } from '@material-ui/core/TextField';

const InputText: React.FC<TextFieldProps> = ({
  select,
  color,
  variant,
  label,
  placeholder,
  defaultValue,
  value,
  required,
  disabled,
  error,
  helperText,
  name,
  id,
  onChange,
  multiline,
  rows,
  rowsMax,
  InputLabelProps,
  InputProps,
  SelectProps,
  inputProps,
  fullWidth,
}) => {
  // https://github.com/microsoft/TypeScript/issues/8289
  // https://github.com/mui-org/material-ui/issues/15697
  let variantFix: any;
  if (variant === 'outlined') variantFix = 'outlined' as 'outlined';
  if (variant === 'filled') variantFix = 'filled' as 'filled';
  if (variant === 'standard') variantFix = 'standard' as 'standard';

  return (
    <TextField
      select={select}
      color={color}
      variant={variantFix}
      label={label}
      placeholder={placeholder}
      defaultValue={defaultValue}
      value={value}
      required={required}
      disabled={disabled}
      error={error}
      helperText={helperText}
      name={name}
      id={id}
      onChange={onChange}
      multiline={multiline}
      rows={rows}
      rowsMax={rowsMax}
      InputLabelProps={InputLabelProps}
      InputProps={InputProps}
      SelectProps={SelectProps}
      inputProps={inputProps}
      fullWidth={fullWidth}
    />
  );
};

export default InputText;
