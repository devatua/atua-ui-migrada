import React from 'react';

import { AppBar } from '@material-ui/core';
import { AppBarProps } from '@material-ui/core/AppBar';

const appBar: React.FC<AppBarProps & React.ReactNode> = ({ children, color, position }) => (
  <AppBar color={color} position={position}>
    {children}
  </AppBar>
);

export default appBar;
