import React from 'react';

import { Typography } from '@material-ui/core';
import { TypographyProps } from '@material-ui/core/Typography';

const typography: React.FC<React.ReactNode & TypographyProps> = ({
  children,
  variant,
  component,
  color,
  align,
  display,
  style,
  noWrap,
}) => (
  <Typography
    variant={variant}
    component={component}
    color={color}
    align={align}
    display={display}
    style={style}
    noWrap={noWrap}
  >
    {children}
  </Typography>
);

export default typography;
