import React from 'react';

import { Button } from '@material-ui/core';
import { ButtonProps } from '@material-ui/core/Button';

const button: React.FC<React.ReactNode & ButtonProps> = ({
  children,
  color,
  disabled,
  disableFocusRipple,
  disableRipple,
  endIcon,
  fullWidth,
  href,
  size,
  startIcon,
  variant,
  onClick,
}) => (
  <Button
    color={color}
    disabled={disabled}
    disableFocusRipple={disableFocusRipple}
    disableRipple={disableRipple}
    endIcon={endIcon}
    fullWidth={fullWidth}
    href={href}
    size={size}
    startIcon={startIcon}
    variant={variant}
    onClick={onClick}
  >
    {children}
  </Button>
);

export default button;
