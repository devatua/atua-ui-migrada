import React from 'react';
import { TextFieldProps } from '@material-ui/core/TextField';
import { MenuItem, TextField } from '@material-ui/core';

type Props = {
  RenderComponent?: React.ComponentType<TextFieldProps>;
  values?: {
    value?: string | number;
    description?: string;
  }[];
};

const selectText: React.FC<Omit<TextFieldProps, 'select'> & Props> = ({
  color,
  variant,
  label,
  placeholder,
  defaultValue,
  value,
  required,
  disabled,
  error,
  helperText,
  name,
  id,
  onChange,
  multiline,
  rows,
  rowsMax,
  InputLabelProps,
  InputProps,
  SelectProps,
  inputProps,
  values,
  fullWidth,
}) => {
  // https://github.com/microsoft/TypeScript/issues/8289
  // https://github.com/mui-org/material-ui/issues/15697
  let variantFix: any;
  if (variant === 'outlined') variantFix = 'outlined' as 'outlined';
  if (variant === 'filled') variantFix = 'filled' as 'filled';
  if (variant === 'standard') variantFix = 'standard' as 'standard';

  return (
    <TextField
      select
      color={color}
      variant={variantFix}
      fullWidth={fullWidth}
      label={label}
      placeholder={placeholder}
      defaultValue={defaultValue}
      value={value}
      required={required}
      disabled={disabled}
      error={error}
      helperText={helperText}
      name={name}
      id={id}
      onChange={onChange}
      multiline={multiline}
      rows={rows}
      rowsMax={rowsMax}
      InputLabelProps={InputLabelProps}
      InputProps={InputProps}
      SelectProps={SelectProps}
      inputProps={inputProps}
    >
      {(values || []).map((value, index) => (
        <MenuItem key={index} value={value.value}>
          {value.description}
        </MenuItem>
      ))}
    </TextField>
  );
};

export default selectText;
