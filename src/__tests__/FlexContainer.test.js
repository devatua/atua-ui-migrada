import { mount } from 'enzyme';
import React from 'react';
import FlexContainer from '../components/Html/Container/FlexContainer/FlexContainer';

describe('Testando componente FlexContainer', () => {
  it('Deve renderizar corretamente', () => {
    const wrapper = mount(<FlexContainer />);

    expect(wrapper).toMatchSnapshot();
    wrapper.unmount();
  });
});
