import { mount } from 'enzyme';
import React from 'react';
import Header from '../components/Html/Semantic/Header/Header';

describe('Testando componente Header', () => {
  it('Deve renderizar corretamente', () => {
    const wrapper = mount(<Header />);

    expect(wrapper).toMatchSnapshot();
    wrapper.unmount();
  });
});
