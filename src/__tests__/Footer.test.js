import { mount } from 'enzyme';
import React from 'react';
import Footer from '../components/Html/Semantic/Footer/Footer';

describe('Testando componente Footer', () => {
  it('Deve renderizar corretamente', () => {
    const wrapper = mount(<Footer />);

    expect(wrapper).toMatchSnapshot();
    wrapper.unmount();
  });
});
