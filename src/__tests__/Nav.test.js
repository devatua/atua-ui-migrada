import { mount } from 'enzyme';
import React from 'react';
import Nav from '../components/Html/Semantic/Nav/Nav';

describe('Testando componente Nav', () => {
  it('Deve renderizar corretamente', () => {
    const wrapper = mount(<Nav />);

    expect(wrapper).toMatchSnapshot();
    wrapper.unmount();
  });
});
