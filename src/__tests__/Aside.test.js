import { mount } from 'enzyme';
import React from 'react';
import Aside from '../components/Html/Semantic/Aside/Aside';

describe('Testando componente Aside', () => {
  it('Deve renderizar corretamente', () => {
    const wrapper = mount(<Aside />);

    expect(wrapper).toMatchSnapshot();
    wrapper.unmount();
  });
});
