import { mount } from 'enzyme';
import React from 'react';
import Main from '../components/Html/Semantic/Main/Main';

describe('Testando componente Main', () => {
  it('Deve renderizar corretamente', () => {
    const wrapper = mount(<Main />);

    expect(wrapper).toMatchSnapshot();
    wrapper.unmount();
  });
});
