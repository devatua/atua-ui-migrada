import { mount } from 'enzyme';
import React from 'react';
import Form from '../components/Html/Form/Form';

describe('Testando componente Form', () => {
  it('Deve renderizar corretamente', () => {
    const wrapper = mount(<Form />);

    expect(wrapper).toMatchSnapshot();
    wrapper.unmount();
  });
});
