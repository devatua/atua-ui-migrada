import { mount } from 'enzyme';
import React from 'react';
import Container from '../components/Html/Container/Container';

describe('Testando componente Container', () => {
  it('Deve renderizar corretamente com display block', () => {
    const wrapper = mount(<Container display="block" />);

    //testando propriedade
    expect(wrapper.prop('display')).toEqual('block');

    expect(wrapper).toMatchSnapshot();
    wrapper.unmount();
  });

  it('Deve renderizar corretamente com display block e position absolute', () => {
    const wrapper = mount(<Container display="block" position="absolute" />);

    //testando propriedade
    expect(wrapper.prop('display')).toEqual('block');
    expect(wrapper.prop('position')).toEqual('absolute');

    expect(wrapper).toMatchSnapshot();
    wrapper.unmount();
  });
});
