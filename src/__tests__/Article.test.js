import { mount } from 'enzyme';
import React from 'react';
import Article from '../components/Html/Semantic/Article/Article';

describe('Testando componente Article', () => {
  it('Deve renderizar corretamente', () => {
    const wrapper = mount(<Article />);

    expect(wrapper).toMatchSnapshot();
    wrapper.unmount();
  });
});
