import React from 'react';

import { InputText, useTheme } from '../../../../components/MaterialUI';
import { SearchRounded } from '../../../../components/MaterialIcons';

import { changeEventHandler } from '../../../../components/Support';
import { FlexContainer } from '../../../../components/Html';

const Search: React.FC = () => {
  const [search, setSearch] = React.useState<string>('');
  const theme = useTheme();

  return (
    <FlexContainer
      flexDirection={'row'}
      alignItems={'center'}
      width={'300px'}
      background={theme.palette.primary.contrastText}
      style={{
        opacity: '.7',
        color: theme.palette.primary.main,
      }}
    >
      <FlexContainer flexDirection={'row'} justifyContent={'center'} width={'40px'}>
        <SearchRounded />
      </FlexContainer>
      <InputText
        label={'Search'}
        variant={'filled'}
        value={search}
        onChange={(e): void => changeEventHandler(e, setSearch)}
        inputProps={{
          type: 'search',
        }}
        fullWidth
      />
    </FlexContainer>
  );
};

export default Search;
