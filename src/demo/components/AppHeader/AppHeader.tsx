import React from 'react';

/*
atua-ui imports
 */
import { FlexContainer, Header } from '../../../components/Html';
import { AppBar, Button } from '../../../components/MaterialUI';
import { MenuRounded } from '../../../components/MaterialIcons';

import Logo from '../Logo/Logo';
import Search from './Search/Search';

const appHeader: React.FC = () => (
  <Header>
    <AppBar color={'primary'}>
      <FlexContainer
        flexDirection={'row'}
        justifyContent={'space-between'}
        alignContent={'center'}
        alignItems={'center'}
        padding={'15px 15px'}
      >
        <FlexContainer flexDirection={'row'} alignItems={'center'} justifyContent={'space-between'} width={'170px'}>
          <Button variant={'text'} color={'inherit'}>
            <MenuRounded />
          </Button>
          <Logo />
        </FlexContainer>
        <Search />
      </FlexContainer>
    </AppBar>
  </Header>
);

export default appHeader;
