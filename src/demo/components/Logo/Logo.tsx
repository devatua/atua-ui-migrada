import React from 'react';

/*
atua-ui imports
 */
import { FlexContainer } from '../../../components/Html';

import LogoText from './LogoText/LogoText';

const logo: React.FC = () => (
  <React.Fragment>
    <FlexContainer flexDirection={'row'}>
      <LogoText fontWeight={300}>Atua</LogoText>
      <LogoText fontWeight={400}>UI</LogoText>
    </FlexContainer>
  </React.Fragment>
);

export default logo;
