import React from 'react';
import { FontWeightProperty } from 'csstype';

/*
atua-ui imports
 */
import { Typography } from '../../../../components/MaterialUI';

type TextProps = {
  fontWeight: FontWeightProperty;
  children: React.ReactNode;
};

const logoText: React.FC<TextProps> = ({ fontWeight, children }) => (
  <Typography
    component={'div'}
    style={{
      fontSize: '30px',
      fontWeight: fontWeight,
      padding: '0 0 0 5px',
    }}
  >
    {children}
  </Typography>
);

export default logoText;
