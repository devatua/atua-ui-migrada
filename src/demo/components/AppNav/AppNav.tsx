import React from 'react';

/*
atua-ui imports
 */
import { Nav } from '../../../components/Html';

const appNav: React.FC = () => <Nav style={{ display: 'none' }}>add a drawer</Nav>;

export default appNav;
