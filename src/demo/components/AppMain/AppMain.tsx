import React from 'react';

/*
atua-ui imports
 */
import { Main, FlexContainer, Container } from '../../../components/Html';
import { changeEventHandler } from '../../../components/Support';
import { SelectText } from '../../../components/MaterialUI';

const AppMain: React.FC = () => {
  const [select, setSelectValue] = React.useState<string>('');

  const values = [
    {
      value: '',
      description: '',
    },
    {
      value: '0',
      description: 'Hello',
    },
    {
      value: '1',
      description: 'There',
    },
    {
      value: '2',
      description: 'Awesome',
    },
    {
      value: '3',
      description: 'People',
    },
  ];

  return (
    <Main
      style={{
        width: '100%',
      }}
    >
      <FlexContainer
        flexDirection={'column'}
        alignItems={'center'}
        justifyContent={'center'}
        width={'100%'}
        minHeight={'100px'}
      >
        <Container display={'inline'} width={'300px'}>
          <SelectText
            label={'Select'}
            variant={'outlined'}
            value={select}
            onChange={e => changeEventHandler(e, setSelectValue)}
            values={values}
            fullWidth
          />
        </Container>
      </FlexContainer>
    </Main>
  );
};

export default AppMain;
