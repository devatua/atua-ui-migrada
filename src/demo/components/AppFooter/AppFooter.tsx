import React from 'react';

/*
atua-ui imports
 */
import { Footer, FlexContainer } from '../../../components/Html';
import { useTheme, MaterialTheme, Typography } from '../../../components/MaterialUI';

const AppFooter: React.FC = () => {
  const theme = useTheme<MaterialTheme>();

  return (
    <Footer
      style={{
        position: 'absolute',
        bottom: 0,
        left: 0,
        width: '100%',
        height: '100px',
        background: theme.palette.primary.light,
      }}
    >
      <FlexContainer
        flexDirection={'column'}
        alignItems={'center'}
        justifyContent={'center'}
        height={'100%'}
        style={{
          color: theme.palette.primary.contrastText,
          fontSize: '18px',
        }}
      >
        <Typography variant={'body1'}>Contact us on atua-ui@atua.com.br</Typography>
      </FlexContainer>
    </Footer>
  );
};

export default AppFooter;
