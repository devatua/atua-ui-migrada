import React from 'react';

/*
atua-ui imports
 */
import { FlexContainer } from '../components/Html/';
import { ThemeProvider } from '../components/MaterialUI';

import AppHeader from './components/AppHeader/AppHeader';
import AppNav from './components/AppNav/AppNav';
import AppMain from './components/AppMain/AppMain';
import AppFooter from './components/AppFooter/AppFooter';

const app: React.FC = () => (
  <ThemeProvider>
    <FlexContainer flexDirection={'row'} margin={'80px 0 0 0'} width={'100vw'} padding={'15px'}>
      <AppHeader />
      <AppNav />
      <AppMain />
      <AppFooter />
    </FlexContainer>
  </ThemeProvider>
);

export default app;
